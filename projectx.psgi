use strict;
use warnings;

use ProjectX;

my $app = ProjectX->apply_default_middlewares(ProjectX->psgi_app);
$app;

